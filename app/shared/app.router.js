(function () {
    "use strict";
    var homeCtrl = function () {
        $('.transition-a').transition({
            animation: 'slide right in',
            duration: 800,
            interval: 100
        });

        $('.image.dimmable')
            .dimmer({
                on: 'hover'
            });
    };

    var app = angular.module('k-tailors');

    app.config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/home");

        $stateProvider
            .state('home', {
                url: "/home",
                templateUrl: "views/home.html",
                controller: homeCtrl
            })
            .state('about', {
                url: "/about",
                templateUrl: "views/about.html"
            })
            .state('shop', {
                url: "/shop",
                templateUrl: "views/shop.html"
            })
            .state('location', {
                url: "/location",
                templateUrl: "views/location.html"
            })
            .state('blog', {
                url:"/blog",
                templateUrl: "views/post.html"
            })
            .state('blog.post', {
                url: "/post/:id",
                templateUrl: "views/post.html"
            })
    });
})();
