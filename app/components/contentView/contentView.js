(function () {
    var app = angular.module('k-tailors');

    function contentCtrl($stateParams) {
        this.stateParams = $stateParams;
    }

    app.component('contentView', {
        controller: contentCtrl,
        templateUrl: 'app/components/contentView/contentView.html'
    })
})();
