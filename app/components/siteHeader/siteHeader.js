(function() {
    var app = angular.module('k-tailors');
    function headerCtrl() {
        $('.ui.dropdown').dropdown();
        
    }
    
    app.component('siteHeader', {
        controller: headerCtrl,
        templateUrl: 'app/components/siteHeader/siteHeader.html'
    })
})();